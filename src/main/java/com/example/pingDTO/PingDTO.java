package com.example.pingDTO;

import lombok.Data;

@Data
public class PingDTO {
	String url;
	String expected_content;

}
