package com.example;


import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

import com.example.validation.PingRequestValidation;
/*
 *  It seems that instead of making an MVC application I 
 *  can simply have a Spring shell application
 *  http://projects.spring.io/spring-shell/
 *  There are several advantages for this decision:
 *  
 *  1-Get rid of Model which I have never used
 *  2-There is no special need for View because the application is so simple
 *  3-Terminating the application using command line would be easier with Ctrl+c.
 * 
 */



@SpringBootApplication
public class PingTaskApplication {

	public static void main(String[] args) throws Exception {
	
	SpringApplication.run(PingTaskApplication.class, args);
	
		
	
		
		
		
		
	}
}
