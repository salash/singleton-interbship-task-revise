package com.example.services;


import com.messente.Messente;
import com.messente.response.MessenteResponse;

public class SendSMS {

    public static final String API_USERNAME = "API_USERNAME";
    public static final String API_PASSWORD = "API_PASSWORD";

    public static final String SMS_SENDER_ID = "+37256641168";
    public static final String SMS_RECIPIENT = "+37258921909";
    public static final String SMS_TEXT = "Hey! Your website is down, do something";

    public SendSMS() {

        // Create Messente client
        Messente messente = new Messente(API_USERNAME, API_PASSWORD);

        // Create response object
        MessenteResponse response = null;

        try {
            // Send SMS
            response = messente.sendSMS(SMS_SENDER_ID, SMS_RECIPIENT, SMS_TEXT);

            // Checking the response status
            if (response.isSuccess()) {

                // Get Messente server full response
                System.out.println("Server response: " + response.getResponse());

                //Get unique message ID part of the response(can be used for retrieving message delivery status later)
                System.out.println("SMS unique ID: " + response.getResult());

            } else {
                // In case of failure get failure message                
                throw new RuntimeException(response.getResponseMessage());
            }
        } catch (Exception e) {
            System.err.println(e.getMessage());
            throw new RuntimeException("Failed to send SMS! " + e.getMessage());
        }

    }
}