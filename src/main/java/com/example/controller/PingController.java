package com.example.controller;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;

import org.springframework.web.bind.annotation.RequestMethod;

import com.example.pingDTO.PingDTO;
import com.example.readConfig.URI;

import  com.example.services.SchedulerService;
import com.example.validation.PingRequestValidation;


@Controller
public class PingController {
	
	@RequestMapping("/index")
	public String Ping(Model model){
		
		 model.addAttribute("ping", new PingDTO());
		return "ping/index";
	}
	
	@RequestMapping(value="/update", method=RequestMethod.POST)
	public String updatePing(@ModelAttribute PingDTO pingDTO) throws  Exception{
		System.out.println("URI:"+pingDTO.getUrl()+"    "+" Expected_String: "+pingDTO.getExpected_content());
		PingRequestValidation pv=new PingRequestValidation();
		if(pv.checkPingRequestValidation(pingDTO.getUrl())){
			
			new URI(pingDTO.getUrl(),pingDTO.getExpected_content()); // Write on the uri.txt
					
			SchedulerService scheduler=new SchedulerService();
			scheduler.scheduleManager(); 
			return "fragments/success";
		}
		else
			return "fragments/failure";	
	}

}
