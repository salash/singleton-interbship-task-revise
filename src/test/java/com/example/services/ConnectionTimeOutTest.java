package com.example.services;

import java.net.HttpURLConnection;
import java.net.SocketTimeoutException;    
import java.net.URL;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mockito;
import org.powermock.api.mockito.PowerMockito;
import org.powermock.core.classloader.annotations.PrepareForTest;
import org.powermock.modules.junit4.PowerMockRunner;

import static org.junit.Assert.*;

@RunWith(PowerMockRunner.class)

@PrepareForTest(PingService.class) 
public class ConnectionTimeOutTest {

String url = "http://example.com";
@Test
public void timeout() throws Exception{

    URL mockURL = PowerMockito.mock(URL.class);
    HttpURLConnection mockConnection = PowerMockito.mock(HttpURLConnection.class);

    PowerMockito.whenNew(URL.class).withArguments(url).thenReturn(mockURL);

    PowerMockito.when(mockURL.openConnection()).thenReturn(mockConnection);

    SocketTimeoutException expectedException = new SocketTimeoutException();

    PowerMockito.when(mockConnection.getResponseCode()).thenThrow(expectedException);

    PingService cut = new PingService();

    boolean result=cut.checkSuccess(cut.sendHeadRequest(url,7000.0), "example");
    System.out.println(result);
    assertFalse(result);

    Mockito.verify(mockConnection).setRequestMethod("HEAD");
    Mockito.verify(mockConnection).setConnectTimeout(7000);

}
}
